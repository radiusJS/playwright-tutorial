from playwright.sync_api import Page

class DuckDuckGoSearchPage:

    URL = 'https://www.duckduckgo.com' # better read it from config file, avoid hard code

    def __init__(self, page: Page) -> None:
        self.page = page
        self.search_button = page.locator('id=search_button_homepage')
        self.search_input = page.locator('id=search_form_input_homepage')


    def load(self) -> None:
        self.page.goto(self.URL)
    
    def search(self, phrase: str) -> None:
        self.search_input.fill(phrase)
        self.search_button.click()