import pytest 

from pages.search import DuckDuckGoSearchPage
from pages.result import DuckDuckGoResultPage
from playwright.sync_api import Page, expect

ANIMALS = [
    'panda',
    'python',
    'parrot',
    'leon'
]

@pytest.mark.parametrize('phrase', ANIMALS)
def test_basic_duckduckgo_search(
        phrase: str,
        page: Page,
        search_page: DuckDuckGoSearchPage,
        result_page: DuckDuckGoResultPage) -> None:
    
    # Given the DuckDuckGo home is displayed
    search_page.load()

    # When the user search for panda
    search_page.search(phrase)
    
    # Then the search result query is the pharse
    expect(result_page.search_input).to_have_value(phrase)
    
    # And the search result links pertain to the phrase

    # have at least on match
    # make the test littel bit robust, because we not control the input data
    assert result_page.result_link_titles_contain_phrase(phrase)

    # And the search result title contains the phrase
    # we dont need a locator because we have direct acess to the tab tilte
    # using expect....we have a wait until the titel may changed by some javascript
    # after rendering
    expect(page).to_have_title(f'{phrase} at DuckDuckGo')
