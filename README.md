## Screenshots / Video

Make test for every test case.

`python -m pytest tests --screenshot on`

Make full video

`python -m pytest tests --video on`

Make video only on failure

`python -m pytest tests --screenshot only-on-failure` 

## Run test parallel

- shard state and shard user account is to consider
- may use docker container with test data run alle parallel tests
- on per test run per core
- pytest paralle `pip install pytest-xdist`
- `python -m pytest --headed --verbose -n 4`
- on parralel test some times it fails
- we can run faild test twise to make it more reliable
- Tip: small number of reliable tests